# WallPaperEngine
A, so far, simple HolyC Wallpaper engine for TempleOS

Probably soon to come:
* Options for colors White, Brown, and Yellow (not added yet because I was lazy due to an interference issue)

Maybe? to come:
* Option to draw image(s) to use for Wallpaper decoration, in addition to the main color change
* Interactive menu, lacking the need for typing options (might need to take a whole different approach)

### Use:
`WallPaperEngine.HC` is the main file, so in TempleOS, just press `F5` while in the file, to quickly compile and run it.
It will ask for a color option to put input verbatim, and then reboot to display changes.

Also for recreation, it may be better to simply Find; (& Replace All) copies of `WallPaper.HC` from `::/Adam/(WallPaper.HC)` for each of the colors in their respective directories, since some things like sprites are supported to be stored only within TempleOS.

Here is an example of this:
```
// BLUE (Default) >> BLACK (Example)

Find("BLUE", "WallPaper.HC", , "BLACK"); // Third option irrelevant to our case
// Then choose to "replace all"
```

While this repo currently stands simply as reference, this would be the best option for recreation, although I will say ~~I am working on a method to allow installation of such projects to TempleOS, hopefully to be available soon.~~

~~With this method in the future, compressed HolyC files should be able to be installed to your TempleOS, making setup/installation much easier and accessible.~~

### UPDATE:
See my `temple/` repository for easy setup.